<?php declare(strict_types=1);

namespace Eternaltwin\PathTools;

use Eternaltwin\PathTools\PathTools;

final class Posix extends PathTools {
  const SEPARATOR = "/";

  public static function join(string ...$paths): string {
    return self::normalize(self::concat(...$paths));
  }

  public static function relative(string $from, string $to): string {
    if (!self::isAbsolute($from) || !self::isAbsolute($to)) {
      throw new \Error("Arguements to `PathTools::relative` must be absolute, got: " . $from . " and " . $to);
    }
    $from = self::normalize($from);
    $to = self::normalize($to);
    if ($from == $to) {
      return ".";
    }
    $fromParts = explode(self::SEPARATOR, $from);
    $toParts = explode(self::SEPARATOR, $to);
    $fromParts = array_values(array_filter($fromParts, function($p) { return !empty($p); }));
    $toParts = array_values(array_filter($toParts, function($p) { return !empty($p); }));
    $common = 0;
    $fromLen = count($fromParts);
    $toLen = count($toParts);
    while ($common < $fromLen && $common < $toLen && $fromParts[$common] === $toParts[$common]) {
      $common += 1;
    }
    $parentCount = $fromLen - $common;
    $prefix = $parentCount > 0 ? str_repeat("../", $parentCount) : "./";
    if ($common === $toLen) {
      $prefix = substr($prefix, 0, strlen($prefix) - 1);
    }
    return $prefix . implode(self::SEPARATOR, array_slice($toParts, $common));
  }

  public static function concat(string ...$paths): string {
    if (empty($paths)) {
      return ".";
    }
    $result = [];
    foreach ($paths as $path) {
      if (!empty($path)) {
        $result[] = $path;
      }
    }
    if (empty($result)) {
      return ".";
    }
    return implode(self::SEPARATOR, $result);
  }

  public static function normalize(string $path): string {
    if (empty($path)) {
      return ".";
    }
    $isAbsolute = self::isAbsolute($path);
    $trailingSeparator = str_ends_with($path, self::SEPARATOR);
    $normalized = self::normalizeInner($path, self::SEPARATOR, !$isAbsolute, function($chr) { return $chr === self::SEPARATOR; });
    if (empty($normalized)) {
      if ($isAbsolute) {
        return self::SEPARATOR;
      } elseif ($trailingSeparator) {
        return "./";
      } else {
        return ".";
      }
    }
    if ($isAbsolute) {
      $normalized = self::SEPARATOR . $normalized;
    }
    if ($trailingSeparator) {
      $normalized = $normalized . self::SEPARATOR;
    }
    return $normalized;
  }

  public static function resolve(string $base, $path): string {

  }

  public static function isAbsolute(string $path): bool {
    return str_starts_with($path, self::SEPARATOR);
  }
}
